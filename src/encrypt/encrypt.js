const SIZE = 5;

function SimpleEncrypt(key, size = SIZE) {
  this.encode = (data) => {
    return data
      .split('')
      .map(symbol => symbol.charCodeAt() ^ key)
      .map(symbol => String(symbol).padStart(SIZE, 0))
      .join('');
  }
  
  this.decode = (data) => {
    var regexp = new RegExp(`.{1,${SIZE}}`, 'g');
    const items = data.match(regexp);
    return items
      .map(symbol => String.fromCharCode((+symbol) ^ key))
      .join('');
  }
}

module.exports = SimpleEncrypt;