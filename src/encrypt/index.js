const SimpleEncrypt = require('./encrypt.js');

const encrypt = new SimpleEncrypt(122);

const data = 'моя строка';
const encodedData = encrypt.encode(data);
const decodedData = encrypt.decode(encodedData);

console.log(data);
console.log(encodedData);
console.log(decodedData);