const isAllow = require('./permissions').getPermissionsFunctions();

class Data {
  #permissions = 0;
  #items = [];

  constructor(permissions) {
    this.#permissions = permissions;
  }

  read(index) {
    if(!isAllow.read(this.#permissions)) throw 'Доступ к чтению закрыт';
    return this.#items[index]
  }

  add(value) {
    if(!isAllow.add(this.#permissions)) throw 'Доступ к добавлению закрыт';
    this.#items.push(value);
  }

  edit(index, value) {
    if(!isAllow.edit(this.#permissions)) throw 'Доступ к редактированию закрыт';
    this.#items[index] = value;
  }

  remove(index) {
    if(!isAllow.remove(this.#permissions)) throw 'Доступ к редактированию закрыт';
    this.#items.splice(index, 1);
  }

  clear() {
    if(!isAllow.clear(this.#permissions)) throw 'Доступ к очистке закрыт';
    this.#items = [];
  }

  getItems() {
    return this.#items;
  }
}

module.exports = {
  Data
}