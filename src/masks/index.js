const { getPermissionsValue } = require('./permissions');
const { Data } = require('./data');

const myRights = getPermissionsValue(true, true, true, true, false);

const data = new Data(myRights);

data.add(1);
data.add(2);
data.add(3);

console.log(data.getItems());
console.log(data.read(2));

data.edit(0, 10);
data.remove(1);

console.log(data.getItems());

data.clear();
