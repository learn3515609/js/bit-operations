const PERMISSIONS = {
  READ: 1,
  ADD: 2,
  EDIT: 4,
  REMOVE: 8,
  CLEAR: 16
}

const getPermissionsValue = (
  read = false ,
  add = false,
  edit = false,
  remove = false,
  clear = false
) => {
  return (
    (read && PERMISSIONS.READ) | 
    (add && PERMISSIONS.ADD) | 
    (edit && PERMISSIONS.EDIT) | 
    (remove && PERMISSIONS.REMOVE) | 
    (clear && PERMISSIONS.CLEAR)
  )
}

const getPermissionsFunctions = () => {
  return {
    read: (permissions) => Boolean(permissions & PERMISSIONS.READ),
    add: (permissions) => Boolean(permissions & PERMISSIONS.ADD),
    edit: (permissions) => Boolean(permissions & PERMISSIONS.EDIT),
    remove: (permissions) => Boolean(permissions & PERMISSIONS.REMOVE),
    clear: (permissions) => Boolean(permissions & PERMISSIONS.CLEAR),
  }
}

module.exports = {
  getPermissionsValue,
  getPermissionsFunctions
}