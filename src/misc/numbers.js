const isEven = (number) => !(number & 1)

console.log(isEven(1))
console.log(isEven(2))
console.log(isEven(5))
console.log(isEven(100))

console.log(100 >>> 0)