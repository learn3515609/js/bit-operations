const getBit = (number, index) => {
  return (number >>> index) & 1;
}

const setBit = (number, index) => {
  return number | (1 << index);
}

const clearBit = (number, index) => {
  return number & (~(1 << index));
}

const multipleByTwo = (number, power = 1) => {
  return number << power;
}

const divideByTwo = (number, power = 1) => {
  return number >> power;
}

const countBits = (number) => {
  let count = 0;
  while(number) {
    count += number & 1;
    number >>>= 1;
  }
  return count;
}

const isPowerOfTwo = (number) => {
  return (countBits(number) === 1)
}

let a = 4;
console.log(a, getBit(a, 1));
a = setBit(a, 1);
console.log(a, getBit(a, 1));
a = clearBit(a, 1);
console.log(a, getBit(a, 1));
a = multipleByTwo(a);
console.log(a);
a = divideByTwo(a);
console.log(a);

console.log('count bits of 7', countBits(7));

console.log('2 is power of two', isPowerOfTwo(2));
console.log('15 is power of two', isPowerOfTwo(15));
console.log('64 is power of two', isPowerOfTwo(64));