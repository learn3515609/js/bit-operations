const a = -2;
const b = ~a;
const c = -( a + 1 );

console.log(a, b, c);
console.log((-a).toString(2));
console.log(((-a)>>>0).toString(2));

///////////////////////////

const str = 'string';
const search = 'f';

if(~str.indexOf(search)) {
  console.log('найдено');
} else {
  console.log('не найдено');
}

///////////////////////////

const value = -100;

const isNegative = (value) => {
  return Boolean(2**31 & value);
}
console.log(2, isNegative(2));
console.log(-2, isNegative(-2));